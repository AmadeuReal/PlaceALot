//
//  User.swift
//  PlacesAlot
//
//  Created by Amadeu Martos on 17/08/2018.
//  Copyright © 2018 Amadeu Martos. All rights reserved.
//

import Foundation

protocol HumanInterface: class {
	init(phoneNumber: String)
	var phoneNumber: String { get }
}

class User: HumanInterface {
	var phoneNumber: String
	required init(phoneNumber: String) {
		self.phoneNumber = phoneNumber
	}
}

class UserManager {
	
}
