//
//  AppDelegate.swift
//  PlacesAlot
//
//  Created by Amadeu Martos on 17/08/2018.
//  Copyright © 2018 Amadeu Martos. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
}

